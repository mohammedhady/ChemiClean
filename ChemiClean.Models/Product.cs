﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ChemiClean.Models
{
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string SupplierName { get; set; }
        public string Url { get; set; }
        public string MyProperty { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public byte[] DataSheet { get; set; }
        public bool? Availability { get; set; }
        public bool ContentChanged { get; set; }
        public string FileExtension { get; set; }
    }
}
