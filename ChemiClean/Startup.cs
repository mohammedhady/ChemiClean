﻿using System;
using System.Threading.Tasks;
using Hangfire;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(ChemiClean.Startup))]

namespace ChemiClean
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configuration
                .UseSqlServerStorage("ChemiCleanContext");

            app.UseHangfireDashboard();
            app.UseHangfireServer();
        }
    }
}
