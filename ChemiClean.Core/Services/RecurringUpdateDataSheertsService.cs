﻿using Hangfire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemiClean.Core.Services
{
    public class RecurringUpdateDataSheertsService
    {
        public void UpdateDataSheets()
        {
            DataSheetService dataSheetService = new DataSheetService();
            RecurringJob.AddOrUpdate("UpdateDatasheets",
                () => dataSheetService.DownloadAllDataSheets(true),
                Cron.DayInterval(3));


        }
    }
}
