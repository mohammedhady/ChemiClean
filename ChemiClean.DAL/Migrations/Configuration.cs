﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemiClean.DAL.Migrations
{
    public class Configuration : DbMigrationsConfiguration<ChemiCleanContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ChemiCleanContext context)
        {
        }
    }
}
