﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemiClean.Core
{
    public static class FileTypes
    {
        public static List<FileType> GetFileTypes()
        {
            List<FileType> fileTypes = new List<FileType>
            {
                new FileType{FileExtension = ".txt" , FileContentType= "text/plain" },
                new FileType{FileExtension = ".html" , FileContentType= "text/html" },
                new FileType{FileExtension = ".pdf" , FileContentType= "application/pdf" },
                new FileType{FileExtension = ".doc" , FileContentType= "application/msword" },
                new FileType{FileExtension = ".docx" , FileContentType= "application/vnd.openxmlformats-officedocument.wordprocessingml.document" }
            };
            return fileTypes;
        }
    }
    public class FileType
    {
        public string FileExtension { get; set; }
        public string FileContentType { get; set; }
    }
}

