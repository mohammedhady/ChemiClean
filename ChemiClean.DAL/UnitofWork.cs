﻿using ChemiClean.DAL.Repositories;
using ChemiClean.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemiClean.DAL
{
    public class UnitofWork
    {
        private readonly ChemiCleanContext _dbContext;
        public ProductsRepo<Product> ProductsRepo { get; set; }
        public UnitofWork()
        {
            _dbContext = ChemiCleanContext.Create();
            ProductsRepo = new ProductsRepo<Product>(_dbContext);
        }
        public void SaveChanges()
        {
            _dbContext.SaveChanges();

        }
    }
}
