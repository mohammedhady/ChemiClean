﻿$(document).ready(function () {

    function waitMe() {
        $('#waitMeContainer').waitMe({
            effect: 'bounce',
            text: 'Please Wait...',
            waitTime: -1,
            textPos: 'vertical',
        });
    }

    $("#downloadAll").click(function () {
        waitMe();
        $.ajax({
            url: "Home/DownloadAllDataSheets",
            type: "GET",
            success: function (data, textStatus, jqXHR) {
                $('#waitMeContainer').waitMe("hide");
                $("#divProductsList").html(data)
                toastr.success("datasheets uploaded");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#waitMeContainer').waitMe("hide");
                toastr.error("error downloading datasheets")
            }
        });
    });

    $(document).on("click", ".downloadLocalCopy", function (e) {
        waitMe();
        var productId = $(this).data("id");
        $.ajax({
            url: "Home/DownloadDataSheet",
            type: "GET",
            data: { productId: productId },
            success: function (data, textStatus, jqXHR) {
                $('#waitMeContainer').waitMe("hide");
                $("#divProductsList").html(data)
                toastr.success("datasheets uploaded");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#waitMeContainer').waitMe("hide");
                toastr.error("error downloading datasheets")
            }
        });
    });

    $(document).on("click", ".previewLocalCopy", function (e) {
        var productId = $(this).data("id");
        $.ajax({
            url: "Home/DownloadLocalCopy",
            type: "GET",
            data: { productId: productId },
            success: function (data, textStatus, jqXHR) {
                window.open("/DataSheets/" + data, "_blank");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#waitMeContainer').waitMe("hide");
                toastr.error("error downloading datasheets")
            }
        });
    });


});