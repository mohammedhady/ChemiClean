﻿using ChemiClean.Core.Services;
using ChemiClean.DAL;
using ChemiClean.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ChemiClean.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home 
        DataSheetService _dataSheetService = new DataSheetService();

        public ActionResult Index()
        {
            var products = _dataSheetService.GetAllProducts();
            return View(products);
        }

        public ActionResult DownloadAllDataSheets()
        {
            _dataSheetService.DownloadAllDataSheets();
            var products = _dataSheetService.GetAllProducts();
            return View("_ProductsList", products);
        }


        public ActionResult DownloadDataSheet(int productId)
        {
            _dataSheetService.DownloadDataSheetByProductId(productId);
            var products = _dataSheetService.GetAllProducts();
            return View("_ProductsList", products);
        }

        public ActionResult DownloadLocalCopy(int productId)
        {
            string file = _dataSheetService.DownloadDataSheetLocalCopy(productId);
            if (!string.IsNullOrEmpty(file))
                return Json(file, JsonRequestBehavior.AllowGet);
            else
                return new HttpStatusCodeResult(HttpStatusCode.NotFound, "File not found");
        }

    }
}