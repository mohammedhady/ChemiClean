﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ChemiClean.DAL.Repositories
{
    public class ProductsRepo<T> where T : class
    {
        private readonly ChemiCleanContext _db;
        private readonly DbSet<T> _table;
        public ProductsRepo(ChemiCleanContext context)
        {
            _db = context;
            _table = _db.Set<T>();
        }
        public T Find(object id)
        {
            return _table.Find(id);
        }
        public virtual IEnumerable<T> Search(Expression<Func<T, bool>> predicate)
        {
            return _db.Set<T>().Where(predicate);
        }
        public List<T> GetAll()
        {
            return _db.Set<T>().ToList();
        }

    }
}
