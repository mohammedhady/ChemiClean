﻿using ChemiClean.DAL;
using ChemiClean.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace ChemiClean.Core.Services
{
    public class DataSheetService
    {
        UnitofWork _uow = new UnitofWork();
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        public List<Product> GetAllProducts()
        {
            return _uow.ProductsRepo.GetAll();
        }
        public void DownloadAllDataSheets(bool recurringJob =false)
        {
            var products = _uow.ProductsRepo.GetAll();
            foreach (var product in products)
            {
                HttpWebRequest httpRequest = (HttpWebRequest)
                WebRequest.Create(product.Url);
                httpRequest.Method = WebRequestMethods.Http.Get;
                try
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                    if (httpResponse.StatusCode == HttpStatusCode.OK)
                    {
                        Stream httpResponseStream = httpResponse.GetResponseStream();
                        byte[] file = ReadStremToByteArray(httpResponseStream);
                        string fileExtension = GetFileExtension(httpResponse.ContentType);
                        if(recurringJob && product.DataSheet != null)
                        {
                            product.ContentChanged = CheckDataSheetChanged(product.DataSheet, file);
                        }
                        product.Availability = true;
                        product.DataSheet = file;
                        product.FileExtension = fileExtension;
                    }
                    else
                        product.Availability = false;
                }
                catch (WebException ex)
                {
                    product.Availability = false;
                    var httpResponse = (HttpWebResponse)ex.Response;
                    string response = httpResponse != null ? httpResponse.StatusCode.ToString() : "server could not be found";
                    logger.Error($"URL: {product.Url} return: {response}");
                }
                catch (Exception e)
                {
                    logger.Error(e);
                }
            }
            try
            {
                _uow.SaveChanges();
            }
            catch (Exception e)
            {
                logger.Error($"error saving data: {e}");
                throw;
            }
        }
        public void DownloadDataSheetByProductId(int productId,bool recurringJob = false)
        {
            var product = _uow.ProductsRepo.Find(productId);
            HttpWebRequest httpRequest = (HttpWebRequest)
            WebRequest.Create(product.Url);
            httpRequest.Method = WebRequestMethods.Http.Get;
            try
            {
                HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    Stream httpResponseStream = httpResponse.GetResponseStream();
                    byte[] file = ReadStremToByteArray(httpResponseStream);
                    string fileExtension = GetFileExtension(httpResponse.ContentType);
                    product.Availability = true;
                    product.DataSheet = file;
                    product.FileExtension = fileExtension;
                    if (recurringJob && product.DataSheet != null)
                    {
                        product.ContentChanged = CheckDataSheetChanged(product.DataSheet, file);
                    }
                }
                else
                    product.Availability = false;
            }
            catch (WebException ex)
            {
                product.Availability = false;
                var httpResponse = (HttpWebResponse)ex.Response;
                string response = httpResponse != null ? httpResponse.StatusCode.ToString() : "server could not be found";
                logger.Error($"URL: {product.Url} return: {response}");
            }
            catch (Exception e)
            {
                logger.Error(e);
            }
            try
            {
                _uow.SaveChanges();
            }
            catch (Exception e)
            {
                logger.Error($"error saving data: {e}");
                throw;
            }
        }
        public byte[] ReadStremToByteArray(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public string DownloadDataSheetLocalCopy(int productId)
        {
            Product product = _uow.ProductsRepo.Find(productId);
           return SaveBytesToFile(product.SupplierName, product.FileExtension, product.DataSheet);
        }
        public string SaveBytesToFile(string filename, string fileExtension, byte[] bytesToWrite)
        {
            if (filename != null && filename.Length > 0 && bytesToWrite != null)
            {
                string directoryName = HttpContext.Current.Server.MapPath("~/DataSheets");
                string filePath = (Path.Combine(directoryName, $"{filename.Replace(" ", "-")}{fileExtension}"));

                if (!Directory.Exists(Path.GetDirectoryName(filePath)))
                    Directory.CreateDirectory(Path.GetDirectoryName(filePath));

                FileStream file = File.Create(filePath);
                try
                {
                    file.Write(bytesToWrite, 0, bytesToWrite.Length);
                    file.Close();
                    return $"{filename.Replace(" ", "-")}{fileExtension}";
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return string.Empty;
        }

        public string GetFileExtension(string contentType)
        {
            var fileTypes = FileTypes.GetFileTypes();
            string fileExtension = fileTypes.Where(x => x.FileContentType == contentType)
                .Select(x => x.FileExtension)
                .SingleOrDefault();
            return fileExtension;
        }

        public bool CheckDataSheetChanged(byte [] datasheet, byte[] newDatasheet)
        {
            /*
             * i am not sure this is the right way to compare two byte array
            https://docs.microsoft.com/en-us/dotnet/api/system.linq.enumerable.sequenceequal?view=netframework-4.7.2
            */
            return datasheet.SequenceEqual(newDatasheet);
        }

    }
}
