﻿using ChemiClean.DAL.Migrations;
using ChemiClean.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemiClean.DAL
{
    public class ChemiCleanContext :DbContext
    {
        public DbSet<Product> Products { get; set; }

        public ChemiCleanContext() : base("ChemiCleanContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ChemiCleanContext, Configuration>());
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
        public static ChemiCleanContext Create()
        {
            return new ChemiCleanContext();
        }
    }
}
